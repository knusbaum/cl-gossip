(in-package :gossip)

(defclass gossip-server (net-listener)
  ((host :initarg :host
         :initform (error "Must specify host when creating gossip listener")
         :accessor gossip-host)
   (seeds :initarg :seeds
          :initform nil
          :accessor gossip-seeds)
   (period :initarg :period
           :initform 2
           :accessor gossip-period)
   (host-timeout-secs :initarg :period
                      :initform 10
                      :accessor gossip-host-timeout-secs)
   (hosts :initform (make-hash-table :synchronized t :test #'equalp)
          :accessor gossip-hosts)
   (services :initform (make-hash-table :synchronized t :test #'equalp)
             :accessor gossip-services)
   (listen-thread :initform nil
                  :accessor gossip-listen-thread)
   (gossip-thread :initform nil
                  :accessor gossip-gossip-thread))
  (:default-initargs
    :serialized-class 'gossip-protobufs:host-list
    :listen-port 8234
    :handle-msg-func 'merge-hosts))


(defvar *hosts* nil)
(defvar *services* nil)

(defmethod run-listener :around ((g-server gossip-server))
  (let ((*hosts* (gossip-hosts g-server))
        (*services* (gossip-services g-server)))
    (call-next-method)))

;; 60s * 60m * 24h * 365d * 70y -> difference between unix time (1970) and CL time (1900)
(defconstant +UNIVERSAL-UNIX-TIME-DIFF+ (* 60 60 24 365 70))

(defun get-unix-time ()
  (- (get-universal-time) +UNIVERSAL-UNIX-TIME-DIFF+))

(defun hash-keys (hash-table)
  (loop for k being the hash-keys in hash-table collecting k))

(defun host-equal (h1 h2)
  (equalp (gossip-protobufs:hostname h1) (gossip-protobufs:hostname h2)))

(defun merge-hosts (new-hosts)
  (mapcar (lambda (h)
            (let* ((hname (gossip-protobufs:hostname h))
                   (new-ts (gossip-protobufs:unix-tstamp h))
                   (curr-ts (gethash hname *hosts*)))
              (when (or (not curr-ts)
                        (< curr-ts new-ts))
                (setf (gethash hname *hosts*) new-ts)
                (setf (gethash hname *services*) (gossip-protobufs:services h)))))
          (gossip-protobufs:hosts new-hosts)))

(defun close-client (client)
  (iolib:remove-fd-handlers *event-base* (iolib:socket-os-fd client) :error t :write t :read t)
  (remhash client *clients*)
  (close client)) ; just in case

(defun resolve-addr (addr)
  (let ((vec-addr (iolib:string-address-to-vector addr)))
    (if vec-addr
        (iolib:make-address vec-addr)
        (iolib:lookup-hostname addr))))

(defun send-length (len)
  (when (> (ldb (byte 8 32) len) 0)
    (error "Size must fit in 32 bits."))

  (let ((high (ldb (byte 8 24) len))
        (mid-hi (ldb (byte 8 16) len))
        (mid-lo (ldb (byte 8 8) len))
        (low (ldb (byte 8 0) len)))
    (make-array 4 :initial-contents (list high mid-hi mid-lo low))))

(defun send-list-to (hostname self)
  (when (equalp hostname self)
    (return-from send-list-to))
  (handler-case
      (let* ((hosts
              (loop for k being the hash-keys in *hosts*
                 collecting (make-instance 'gossip-protobufs:host :hostname k :unix-tstamp (gethash k *hosts*)
                                           :services (gethash k *services*))))
             (hlist (make-instance 'gossip-protobufs:host-list
                                   :hosts hosts))
             (buf (proto:serialize-object-to-bytes hlist 'gossip-protobufs:host-list)))

        (let ((socket
               (iolib:make-socket
                :connect :active
                :address-family :internet
                :type :stream
                :ipv6 nil)))
          (unwind-protect
               (progn
                 (iolib:connect socket (resolve-addr hostname) :port 8234 :wait t)
                 (iolib:send-to socket (send-length (length buf)))
                 (iolib:send-to socket buf))
            (close socket))))
    (error (e) (format t "Failed to send to server: ~a~%" e))))

(defun send-gossip (host-hash seeds self)
  (let* ((hosts (hash-keys host-hash))
         (hosts-len (length hosts)))
    (when (> hosts-len 0)
      ;; do some gossiping
      (let ((h1 (elt hosts (random hosts-len)))
            (h2 (elt hosts (random hosts-len))))
        (send-list-to h1 self)
        (when (not (equalp h1 h2))
          (send-list-to h2 self))))
    (loop for seed in seeds
       do
         (when (not (gethash seed host-hash))
           ;; Try to connect to a seed.
           (format t "Trying to contact the seed ~a~%" seed)
           (send-list-to seed self)))))

(defun cleanup-hosts (g-server host-hash service-hash)
  (let ((late-time (- (get-unix-time) (gossip-host-timeout-secs g-server))))
    (maphash (lambda (k v)
               (when (< v late-time)
                 (remhash k host-hash)
                 (remhash k service-hash)))
             host-hash)))

(defun run-gossiper* (g-server)
  (let ((*hosts* (gossip-hosts g-server))
        (*services* (gossip-services g-server)))
    (loop do
         (progn
           ;; Make sure if other thread dies, we also die.
           (when (not (bt:thread-alive-p (gossip-listen-thread g-server)))
             (return-from run-gossiper*))

           (sleep (gossip-period g-server))
           (setf (gethash (gossip-host g-server) *hosts*) (get-unix-time))
           (send-gossip *hosts* (gossip-seeds g-server) (gossip-host g-server))
           (cleanup-hosts g-server *hosts* *services*)
           (maphash (lambda (k v)
                      (format t "HOST: ~a~%" k))
                    *hosts*)
           (format t "--------------------------------------------------------------------------------~%")))))

(defun start-server (g-server)
  (setf (exit-condition g-server)
        (lambda () (not (bt:thread-alive-p (gossip-gossip-thread g-server)))))

  (setf (gossip-listen-thread g-server)
        (bt:make-thread (lambda () (run-listener g-server))
                        :name "GOSSIP-SERVER-LISTENER"))
  (setf (gossip-gossip-thread g-server)
        (bt:make-thread (lambda () (run-gossiper* g-server))
                        :name "GOSSIP-SERVER-GOSSIPER"))
  g-server)

(defun stop-server (g-server)
  (bt:destroy-thread (gossip-listen-thread g-server))
  (bt:destroy-thread (gossip-gossip-thread g-server)))

(defun server-running-p (g-server)
  (and (bt:thread-alive-p (gossip-listen-thread g-server))
       (bt:thread-alive-p (gossip-gossip-thread g-server))))

(defun live-hosts (g-server)
  (hash-keys (gossip-hosts g-server)))

(defun host-services (g-server host)
  (mapcar (lambda (srv)
            (cons (gossip-protobufs:name srv) (gossip-protobufs:service-port srv)))
          (gethash host (gossip-services g-server))))

(defun lookup-service (g-server service-name)
  (apply #'concatenate 'list
         (loop
            with services = (gossip-services g-server)
            for k being the hash-keys in services
            for v = (gethash k services)
            collect (loop for service in v
                       when (equal (gossip-protobufs:name service) service-name) collect (cons k (gossip-protobufs:service-port service))))))

(defun all-services (g-server)
  (apply #'concatenate 'list
         (loop
            with services = (gossip-services g-server)
            for k being the hash-keys in services
            for v = (gethash k services)
            collect (loop for service in v
                       collect (list k (gossip-protobufs:name service) (gossip-protobufs:service-port service))))))

(defun set-local-services (g-server services)
  (setf (gethash (gossip-host g-server) (gossip-services g-server))
        (mapcar (lambda (pair)
                  (make-instance 'gossip-protobufs:service :name (car pair) :service-port (cdr pair)))
                services)))
