;;;; cl-gossip.asd

;; For some reason, asdf doesn't load this before it tries to resolve
;; the modules, so errors with "no such module protobuf-file" or somesuch.
(eval-when (:compile-toplevel :load-toplevel :execute)
  (when (not (find-package 'protobufs))
    (asdf:load-system 'cl-protobufs)))
  
(asdf:defsystem #:cl-gossip
  :name "cl-gossip"
  :description "Distributed protocol for liveness testing."
  :license "Public Domain"
  :author "Kyle Nusbaum"
  :depends-on (#:cl-protobufs #:bordeaux-threads #:iolib)
  :components ((:protobuf-file "gossip")
               (:file "cl-gossip-package"
                      :depends-on ("gossip"))
               (:file "net-listener"
                      :depends-on ("cl-gossip-package"))
               (:file "cl-gossip"
                      :depends-on ("cl-gossip-package"))))
