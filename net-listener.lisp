(in-package :gossip)

(defvar *listener*)
(defvar *event-base*)
(defvar *clients* nil)

(defclass net-listener ()
  ((serialized-class
    :initarg :serialized-class
    :initform (error "Must specify a protobufs class when creating a net listener.")
    :accessor serialized-class)
   (listen-address
    :initarg :listen-address
    :initform iolib:+IPV6-UNSPECIFIED+
    :accessor listen-address)
   (listen-port
    :initarg :listen-port
    :initform (error "Must specify a listener port when creating a net listener.")
    :accessor listen-port)
   (listen-timeout
    :initarg :listen-timeout
    :initform 1
    :accessor listen-timeout)
   (exit-condition
    :initarg :exit-condition
    :initform nil
    :accessor exit-condition)
   (handle-msg-func
    :initarg :handle-msg-func
    :initform (error "Must specify a message-handling function when creating a net listener.")
    :accessor handle-msg-func)))

(defclass message-reader ()
  ((length :initform 0 :accessor mr-length)
   (length-bytes-read :initform 0 :accessor mr-lbr)
   (have-length :initform nil :accessor mr-have-length)
   (array :initform nil :accessor mr-array)
   (array-off :initform 0 :accessor mr-array-off)))

(defun close-client (client)
  (iolib:remove-fd-handlers *event-base* (iolib:socket-os-fd client) :error t :write t :read t)
  (remhash client *clients*)
  (close client)) ; just in case

(defun read-length (message-reader sock)
  (let* ((prev-bytes-read (mr-lbr message-reader))
         (len-bytes (make-array (- 4 prev-bytes-read)
                                :element-type 'iolib/base:ub8)))
    (multiple-value-bind
          (same-buffer bytes-read) (iolib:receive-from sock :buffer len-bytes)

      ;; Check if the remote was closed
      (when (= bytes-read 0)
        (close-client sock)
        (return-from read-length))

      (let ((total-read (+ bytes-read prev-bytes-read)))
        (setf (mr-lbr message-reader) total-read)

        (when (= total-read 4)
          (setf (mr-have-length message-reader) t))

        ;; shift the bytes we just got into the length
        (loop
           with len = (mr-length message-reader)
           for i from 0 to (1- bytes-read)
           do (setf len (+ (ash len 8) (elt len-bytes i)))
           finally (setf (mr-length message-reader) len))))))

(defun read-message-body (message-reader client)
  (when (not (mr-array message-reader))
    (setf (mr-array message-reader)
          (make-array (mr-length message-reader) :element-type 'iolib/base:ub8)))

  (let* ((buf (mr-array message-reader))
         (arr-len (mr-length message-reader))
         (arr-off (mr-array-off message-reader))
         (sub-len (- arr-len arr-off)))

    (multiple-value-bind
          (same-buffer bytes-read) (iolib:receive-from client :buffer buf :start arr-off)

      ;; Check for client close
      (when (= bytes-read 0)
        (close-client client)
        (return-from read-message-body))

      ;; Increment array offset
      (setf (mr-array-off message-reader) (+ (mr-array-off message-reader) bytes-read))

      ;; We have the full message. Deserialize and update the hosts.
      (when (= (mr-array-off message-reader) arr-len)
        (let ((obj (proto:deserialize-object-from-bytes (serialized-class *listener*) buf)))
          ;(merge-hosts (gossip-protobufs:hosts h-list)))
          (funcall (handle-msg-func *listener*) obj))
;        (setf (mr-have-length message-reader) nil)
;        (setf (mr-length message-reader) 0)
        (setf (gethash client *clients*) (make-instance 'message-reader))))))
;        (close-client client)))))

(defun make-client-handler (client)
  (lambda (fd event exception)
    (let ((message-reader (gethash client *clients*)))
      (if (not (mr-have-length message-reader))
          (read-length (gethash client *clients*) client)
          (read-message-body message-reader client)))))

(defun make-listener (sock)
  (lambda (fd event exception)
    (let* ((client (iolib:accept-connection sock :wait t)))
      (setf (gethash client *clients*) (make-instance 'message-reader))
      (iolib:set-io-handler *event-base*
                            (iolib:socket-os-fd client)
                            :read
                            (make-client-handler client)))))

(defgeneric run-listener (listener))
(defmethod run-listener ((listener net-listener))
  (let ((sock (iolib:make-socket :local-host (listen-address listener)
                                 :local-port (listen-port listener)
                                 :connect :passive
                                 :ipv6 t)))
    (let ((*event-base* (make-instance 'iolib:event-base))
          (*clients* (make-hash-table :synchronized t))
          (*listener* listener))
      (unwind-protect
           (progn
             (iolib:listen-on sock :backlog 10)
             (iolib:set-io-handler  *event-base*
                                    (iolib:socket-os-fd sock)
                                    :read
                                    (make-listener sock))
             (loop
                do (iolib:event-dispatch *event-base* :timeout (listen-timeout listener))
                until (and (exit-condition listener) (funcall (exit-condition listener)))))

        (maphash (lambda (k v) (close k)) *clients*)
        (close sock)))))
