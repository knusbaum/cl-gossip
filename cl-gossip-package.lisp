(defpackage :gossip
  (:use :common-lisp :gossip-protobufs)
  (:export
   ;; classes
   gossip-server

   ;; functions
   start-server
   stop-server
   server-running-p
   live-hosts
   host-services
   lookup-service
   all-services
   set-local-services))
